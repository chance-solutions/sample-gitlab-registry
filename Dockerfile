# spring boot demo
#
# VERSION               0.0.1

FROM java:8-jre-alpine
VOLUME /tmp
ADD /target/demo-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT exec java -jar app.jar